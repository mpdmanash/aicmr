# Augmentation of Inspection Capabilities using Mixed-Reality
A modern method to perfrom inspection related information sharing in 3D with the Microsoft Hololens.  
This project demonstrates a new system for the purpose of Bridge Inspection and information sharing. The idea is to have a 3D representation of the Bridge and pass all information in 3D coordinates for accurate, precise and intuitive marking of 3D structures for the purpose of inspection.

Demo Video available at: https://www.youtube.com/watch?v=xKZL7sdT6X4


 ---
## About this demo:
- There are 3 components: 
    - A Hololens user inspecting a bridge
    - An inspector at remote location who can view what the Hololens user is marking on the bridge and simultaneously pass new markers to that user.
    - A server to enable communication
- For demo, the server and remoteinspection application are on the same computer.
- A local hotspot is created on the server computer for the hololens to join the same network.
- Hololens uses the local computer's IP address "10.42.0.1" (fixed* as server is hosting the hotspot) to communicate.
- Major proof of concepts:
    - Given an original point cloud of a bridge (template_downsampled.pcd on the server), the algorithm used here can localize the Hololens user scanning the bridge.
    - That registration is further used to share markers between the Hololens user and Remote inspector in 3D in most natural way, making inspection precise and intuitive.
---
## Build Instructions 
### Dependencies
- Eigen
- PCL
- OpenCV
- OpenMP
- c++ Threads with c++0x compiler support
- numpy

### Compile
- `$ cd Server/dependencies/jsoncpp`  
- `$ mkdir -p build`  
- `$ cd build`  
- `$ cmake ..`  
- `$ make`  
- `$ cd ../../../`  
- `$ mkdir build`
- `$ mkdir temp_run_dir`
- `$ cd build`
- `$ cmake ..`
- `$ make`

### Compile and Install HoloLens App
- The HoloLens app is present in `HoloLens/MeshTransferer` directory.
- It was developed and tested in Unity 5.6.0f3 and Visual Studio 2017
- Please follow the build and install instructions as shown in: https://developer.microsoft.com/en-us/windows/mixed-reality/holograms_101e

### Run
- Create local wifi hotspot on the computer following the instructions present in [1] and connect the Hololens to this wifi network.
- `$ cd temp_run_dir` Run all the scripts from a common directory
- `$ ../build/./register_pointclouds ../data/template_downsampled.pcd HololensPC.pcd ../data/kp6_9.json` Run the point cloud registration application
- `$ sudo python ../scripts/MeshServer.py` Run the Server application to receive Mesh from Hololens
- `$ sudo python ../scripts/PointServer.py` Run the Server application to receive point click information between Hololens and Remote Inspector
- `$ ../build/./remoteinspectorRGB ../data/template_downsampled_RGB.pcd` Run the Remote Inspector Application
- Run the "Mesh Transferer" application in the Hololens

## Demo instructions:
- Step 1: **(Mesh Generation)** Start the "Mesh Transferer" app on the Hololens and scan the bridge with it. You would be able to see a Mesh being formed over the 3D structure that you are scanning. Get good scan such that you cover fairly unique structures of the bridge like long structs and joints.
- Step 2: **(Server Registration)** Once you are satisfied with the scan, go ahead and make a "click" gesture anywhere infront of you. This will transmit the mesh generated to the server. Wait for the mesh that you can see on the Hololens to disappear, indicating that registration is successful.
- Step 3: **(Inspection)** Now you may go ahead and click on the 3D structures around you to mark them for inspection purposes. The 3D point where you label will be marked with a "Red Sphere" of radius 0.4 meters. This point is transmitted to server and would be visible on the "remoteinspectorRGB" application.
- Step 4: **(Remote Inspection)** The "remoteinspectorRGB" application is used to visualize points where the Hololens user has clicked on the Original point cloud of the bridge. You may also click on various locations on the point cloud vizualized in this application to add new "Green Spheres" that would then be transmitted back to the Hololens user on his headset.

\* Note: IP address of server is considered "10.42.0.1". This is the IP address that the server will attain if it generates its own local wifi hotspot using the standard instructions to setup hotspot on Ubuntu 16.04.

[1] https://askubuntu.com/questions/762846/how-to-create-wifi-hotspot-in-ubuntu-16-04-since-ap-hotspot-is-no-longer-working

Manash Pratim Das (mpdmanash@iitkgp.ac.in)
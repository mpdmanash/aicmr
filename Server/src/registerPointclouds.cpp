/**
 * registerPointclouds primarily registeres two point clouds
 * It perform disparity computation for the TLS point cloud
 * and waits for communication from the server to signal the
 * arrival of a new point cloud from HoloLens.
 * Then, it perform registration of TLS point cloud and HoloLens
 * point cloud to output the transformation matrix to RT.txt file.
 * 
 * Author: Manash Pratim Das
 * Email: mpdmanash@iitkgp.ac.in
 */

#include "BSC.h"

#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <fstream>
#include <iostream>

void *get_in_addr(struct sockaddr *sa) {
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in *)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}

bool setupSocket(int &listner) {
	// Variables for writing a server.
	/*
	1. Getting the address data structure.
	2. Openning a new socket.
	3. Bind to the socket.
	4. Listen to the socket.
	5. Accept Connection.
	6. Receive Data.
	7. Close Connection.
	*/
	int status;
	struct addrinfo hints, *res;

	// Before using hint you have to make sure that the data structure is
	// empty
	memset(&hints, 0, sizeof hints);
	// Set the attribute for hint
	hints.ai_family = AF_UNSPEC;  // We don't care V4 AF_INET or 6 AF_INET6
	hints.ai_socktype = SOCK_STREAM;  // TCP Socket SOCK_DGRAM
	hints.ai_flags = AI_PASSIVE;

	// Fill the res data structure and make sure that the results make
	// sense.
	status = getaddrinfo(NULL, "8888", &hints, &res);
	if (status != 0) {
		return false;
	}

	// Create Socket and check if error occured afterwards
	listner = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (listner < 0) {
		return false;
	}

	// Bind the socket to the address of my local machine and port number
	status = bind(listner, res->ai_addr, res->ai_addrlen);
	if (status < 0) {
		return false;
	}

	status = listen(listner, 10);
	if (status < 0) {
		return false;
	}

	// Free the res linked list after we are done with it
	freeaddrinfo(res);
	return true;
}

int main(int argc, char *argv[]) {
	int listner;
	if (!setupSocket(listner)) return 0;

	// We should wait now for a connection to accept
	int new_conn_fd;
	struct sockaddr_storage client_addr;
	socklen_t addr_size;
	char s[INET6_ADDRSTRLEN];
	addr_size = sizeof client_addr;

	// ============================ Pre connection setup
	// ====================================
	pcl::PointCloud< pcl::PointXYZ >::Ptr cloud_tu(
	    new pcl::PointCloud< pcl::PointXYZ >);
	pcl::PointCloud< pcl::PointXYZ >::Ptr cloud_t(
	    new pcl::PointCloud< pcl::PointXYZ >);
	pcl::PointCloud< pcl::PointXYZ >::Ptr cloud_qu(
	    new pcl::PointCloud< pcl::PointXYZ >);
	pcl::PointCloud< pcl::PointXYZ >::Ptr cloud_q(
	    new pcl::PointCloud< pcl::PointXYZ >);
	pcl::PointCloud< pcl::PointXYZ >::Ptr cloud_q_aligned(
	    new pcl::PointCloud< pcl::PointXYZ >);
	pcl::PointCloud< pcl::PointXYZRGB >::Ptr display_q(
	    new pcl::PointCloud< pcl::PointXYZRGB >);
	pcl::PointCloud< pcl::PointXYZRGB >::Ptr display_t(
	    new pcl::PointCloud< pcl::PointXYZRGB >);
	pcl::PointCloud< pcl::PointXYZRGB >::Ptr display_r(
	    new pcl::PointCloud< pcl::PointXYZRGB >);
	pcl::KdTreeFLANN< pcl::PointXYZ >::Ptr kdtree_t(
	    new pcl::KdTreeFLANN< pcl::PointXYZ >);
	pcl::KdTreeFLANN< pcl::PointXYZ >::Ptr kdtree_q(
	    new pcl::KdTreeFLANN< pcl::PointXYZ >);
	std::vector< std::vector< bool > > desc_t, desc_q, desc_q2;
	cv::Mat cvdesc_t, cvdesc_q, cvdesc_q2;
	std::vector< int > kps_t, kps_q;
	std::vector< bsc::Vector3f > X_t, X_q, X_q2;

	if (pcl::io::loadPCDFile< pcl::PointXYZ >(argv[1], *cloud_t) == -1) {
		PCL_ERROR("Couldn't read file test_pcd.pcd \n");
		return (-1);
	}
	BSC bsc(200);
	kdtree_t->setInputCloud(cloud_t);

	bsc.ReadKpX(kps_t, X_t, argv[3]);
	bsc.ComputeDescriptors(cloud_t, kdtree_t, kps_t, X_t, 1.3, desc_t);
	bsc.ConvertToCVdescriptors(desc_t, cvdesc_t);
	// ======================================================================================

	while (1) {
		std::cout << "Server now accepting connections ...\n";
		new_conn_fd = accept(listner, (struct sockaddr *)&client_addr,
				     &addr_size);
		if (new_conn_fd < 0) {
			return 0;
		}
		inet_ntop(client_addr.ss_family,
			  get_in_addr((struct sockaddr *)&client_addr), s,
			  sizeof s);
		std::cout << "Server connected to %s \n";
		char buff[1];
		int st = recv(new_conn_fd, buff, 1, MSG_WAITALL);

		if (buff[0] == 'g') {
			// ======================================================================================
			if (pcl::io::loadPCDFile< pcl::PointXYZ >(
				argv[2], *cloud_qu) == -1) {
				PCL_ERROR("Couldn't read file test_pcd.pcd \n");
				return (-1);
			}

			// Register Point Cloud and write transformation matrix
			// to RT.txt
			bsc.Downsample(cloud_qu, 0.3f, cloud_q);
			kdtree_q->setInputCloud(cloud_q);
			bsc.DetectKeypoints(cloud_q, kdtree_q, 0.6, 0.90, kps_q,
					    X_q);
			bsc.InvertXaxes(X_q, X_q2);
			bsc.DrawKeypoints(cloud_q, kps_q, display_q);
			pcl::io::savePCDFileASCII("keypoints_q.pcd",
						  *display_q);
			bsc.ComputeDescriptors(cloud_q, kdtree_q, kps_q, X_q,
					       1.3, desc_q);
			bsc.ConvertToCVdescriptors(desc_q, cvdesc_q);
			bsc.ComputeDescriptors(cloud_q, kdtree_q, kps_q, X_q2,
					       1.3, desc_q2);
			bsc.ConvertToCVdescriptors(desc_q2, cvdesc_q2);
			std::vector< cv::DMatch > matches;
			bsc.MatchCVDescriptors(cvdesc_q, cvdesc_q2, cvdesc_t,
					       matches);
			std::vector< cv::DMatch > gtinliers1, rcinliers,
			    gtinliers2;
			bsc.CompareMatchesWithGT(matches, cloud_q, cloud_t,
						 kps_q, kps_t, gtinliers1);
			std::cout
			    << "Num Inliers: " << gtinliers1.size() << " "
			    << float(gtinliers1.size()) / float(matches.size())
			    << std::endl;

			bsc.RansacInliers(matches, cloud_q, cloud_t, kps_q,
					  kps_t, 0.6, rcinliers);
			bsc.CompareMatchesWithGT(rcinliers, cloud_q, cloud_t,
						 kps_q, kps_t, gtinliers2);
			std::cout << "Num Inliers: " << gtinliers2.size() << " "
				  << float(gtinliers2.size()) /
					 float(rcinliers.size())
				  << std::endl;
			Eigen::Matrix4f trans;
			bsc.FindTransformation(cloud_q, cloud_t, kps_q, kps_t,
					       rcinliers, trans);
			pcl::transformPointCloud(*cloud_q, *cloud_q_aligned,
						 trans);
			std::cout << trans << "\n";

			std::ofstream myfile;
			myfile.open("RT.txt");
			for (int m = 0; m < 4; m++)
				for (int n = 0; n < 4; n++)
					myfile << trans(m, n) << ":";
			myfile.close();

			bsc.DrawRegistration(cloud_q_aligned, cloud_t,
					     display_r);
			pcl::io::savePCDFileASCII("registered.pcd", *display_r);
			kps_q.clear();
			X_q.clear();
			X_q2.clear();
			desc_q.clear();
			cvdesc_q.release();
			desc_q2.clear();
			cvdesc_q2.release();
			// ======================================================================================

			int st = send(new_conn_fd, "RT", 2, 0);
		}

		if (st < 1) {
			close(new_conn_fd);
			_exit(4);
		}
		close(new_conn_fd);
	}

	return 0;
}

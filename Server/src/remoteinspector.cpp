#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/visualization/cloud_viewer.h>
#include <iostream>

#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

int new_conn_fd;
int point_id = 0;
pcl::visualization::PCLVisualizer::Ptr viewer(
    new pcl::visualization::PCLVisualizer);

void* get_in_addr(struct sockaddr* sa) {
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

bool setupSocket(int& sock) {
	struct sockaddr_in server;

	// Create socket
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == -1) {
		return false;
	}

	server.sin_addr.s_addr = inet_addr("0.0.0.0");
	server.sin_family = AF_INET;
	server.sin_port = htons(8889);

	// Connect to remote server
	if (connect(sock, (struct sockaddr*)&server, sizeof(server)) < 0) {
		return false;
	}
	return true;
}

void pp_callback(const pcl::visualization::PointPickingEvent& event,
		 void* in_vd) {
	std::cout << "Picking event active" << std::endl;
	if (event.getPointIndex() != -1) {
		float x, y, z;
		event.getPoint(x, y, z);
		std::cout << x << ";" << y << ";" << z << std::endl;
		std::stringstream ss;
		ss << point_id;
		pcl::PointXYZ positions = pcl::PointXYZ(x, y, z);
		viewer->addSphere< pcl::PointXYZ >(positions, 0.5, 0, 255, 0,
						   ss.str());
		point_id++;
		std::stringstream dataS;
		dataS << positions.x << ':' << positions.y << ':' << positions.z
		      << ':';
		std::string data = dataS.str();
		int st = send(new_conn_fd, data.c_str(), data.length(), 0);
	}
}

void vizSpinner() { viewer->spin(); }

int main(int argc, char** argv) {
	int listner;
	bool sahi = setupSocket(listner);
	if (!sahi) return 0;

	// We should wait now for a connection to accept

	struct sockaddr_storage client_addr;
	socklen_t addr_size;
	char s[INET6_ADDRSTRLEN];
	addr_size = sizeof client_addr;

	// ===================================================================
	pcl::PointCloud< pcl::PointXYZ >::Ptr cloud(
	    new pcl::PointCloud< pcl::PointXYZ >);
	if (pcl::io::loadPCDFile< pcl::PointXYZ >(argv[1], *cloud) == -1) {
		PCL_ERROR("Couldn't read file test_pcd.pcd \n");
		return (-1);
	}

	viewer->setWindowName("Remote Inspection");
	pcl::visualization::PointCloudColorHandlerCustom< pcl::PointXYZ >
	    single_color(cloud, 255, 255, 0);
	viewer->addPointCloud< pcl::PointXYZ >(cloud, single_color, "pc");
	viewer->registerPointPickingCallback(pp_callback, (void*)&viewer);
	std::thread t1(vizSpinner);
	// ===================================================================

	std::cout << "I am now accepting connections ...\n";
	// new_conn_fd = accept(listner, (struct sockaddr *) & client_addr,
	// &addr_size);
	new_conn_fd = listner;

	// inet_ntop(client_addr.ss_family, get_in_addr((struct sockaddr *)
	// &client_addr),s ,sizeof s);
	// std::cout << "I am now connected to %s \n";
	pcl::PointXYZ positions = pcl::PointXYZ(0, 0, 0);
	std::stringstream temp_part;
	int counter = 0;

	while (1) {
		char buff[100];
		// int st = recv(new_conn_fd, buff, 1, MSG_WAITALL);
		int st = recv(new_conn_fd, buff, 100, 0);

		for (int i = 0; i < st; i++) {
			char c = buff[i];
			if (c == '\n') {
				if (counter >= 3) {
					std::cout << "Received: " << positions.x
						  << " " << positions.y << " "
						  << positions.z << "\n";
					std::stringstream ss;
					ss << point_id;
					viewer->addSphere< pcl::PointXYZ >(
					    positions, 0.5, 255, 0, 0,
					    ss.str());
					point_id++;
				}

				counter = 0;
				positions.x = 0.f;
				positions.y = 0.f;
				positions.z = 0.f;
				temp_part.str(std::string());
			} else if (c == ':') {
				if (counter < 3) {
					// temp_part << '\0';
					float temp_pos =
					    std::stof(temp_part.str());
					if (counter == 0)
						positions.x = temp_pos;
					else if (counter == 1)
						positions.y = temp_pos;
					else if (counter == 2)
						positions.z = temp_pos;
					counter++;
					temp_part.str(std::string());
				}
			} else {
				temp_part << c;
			}
		}

		if (st < 1) {
			close(new_conn_fd);
			_exit(4);
		}
	}

	t1.join();
	return 0;
}

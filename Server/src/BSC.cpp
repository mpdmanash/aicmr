/**
 * This is an implementation of a modified Binary Shape Context descriptor based
 * Point Cloud Registration
 * Find more about Binary Shape Context descriptor at
 * https://www.researchgate.net/publication/318445009_A_novel_binary_shape_context_for_3D_local_surface_description
 *
 * Author: Manash Pratim Das
 * Email: mpdmanash@iitkgp.ac.in
 */
#include "BSC.h"

BSC::BSC(int num_pairs, int s) {
	this->m_num_pairs = num_pairs;
	this->m_s = s;
	std::srand((unsigned)std::time(NULL));
	int got_pairs = 0;
	int num_bins = m_s * m_s;
	while (got_pairs < m_num_pairs) {
		int id1 = rand() % num_bins;
		int id2 = rand() % num_bins;
		if (id1 != id2) {
			m_pairs.push_back(std::make_pair(id1, id2));
			got_pairs++;
		}
	}
}

void BSC::Downsample(pcl::PointCloud< pcl::PointXYZ >::Ptr in_cloud,
		     float leaf_size,
		     pcl::PointCloud< pcl::PointXYZ >::Ptr out_cloud) {
	if (bscDEBUG) std::cout << "Downsampling" << std::endl;
	pcl::VoxelGrid< pcl::PointXYZ > sor;
	sor.setInputCloud(in_cloud);
	sor.setLeafSize(leaf_size, leaf_size, leaf_size);
	sor.filter(*out_cloud);
	if (bscDEBUG)
		std::cout << "Downsampled: " << in_cloud->points.size()
			  << " to " << out_cloud->points.size() << std::endl;
}

void BSC::DetectKeypoints(pcl::PointCloud< pcl::PointXYZ >::Ptr in_cloud,
			  pcl::KdTreeFLANN< pcl::PointXYZ >::Ptr kdtree,
			  float search_radius, float threshold,
			  std::vector< int >& out_kps,
			  std::vector< bsc::Vector3f >& out_X) {
	if (bscDEBUG) std::cout << "Detecting Keypoints" << std::endl;
	out_kps.clear();
	out_X.clear();

	int* kp_aloc_id = new int[in_cloud->points.size()]();
	for (int i = 0; i < in_cloud->points.size(); i++) kp_aloc_id[i] = 0;
	std::vector< int > initial_kps;
	std::vector< bsc::Vector2f > initial_evsXY;
	std::vector< bsc::Vector3f > initial_evsXYZ;
	std::vector< bsc::Vector3f > initial_e1V;

	int init_kps_pushed = 0;
	for (int i = 0; i < in_cloud->points.size(); i++) {
		std::vector< int > pointIdxRadiusSearch;
		std::vector< float > pointRadiusSquaredDistance;
		if (kdtree->radiusSearch(in_cloud->points[i], search_radius,
					 pointIdxRadiusSearch,
					 pointRadiusSquaredDistance) > 0) {
			Eigen::MatrixX3d traindataXY(
			    pointIdxRadiusSearch.size(), 3);
			Eigen::MatrixX3d traindataXYZ(
			    pointIdxRadiusSearch.size(), 3);
			for (size_t j = 0; j < pointIdxRadiusSearch.size();
			     ++j) {
				traindataXY(j, 0) =
				    in_cloud->points[pointIdxRadiusSearch[j]].x;
				traindataXY(j, 1) =
				    in_cloud->points[pointIdxRadiusSearch[j]].y;
				traindataXY(j, 2) =
				    0.0;  // in_cloud_s->points[pointIdxRadiusSearch[j]].z;

				traindataXYZ(j, 0) =
				    in_cloud->points[pointIdxRadiusSearch[j]].x;
				traindataXYZ(j, 1) =
				    in_cloud->points[pointIdxRadiusSearch[j]].y;
				traindataXYZ(j, 2) =
				    in_cloud->points[pointIdxRadiusSearch[j]].z;
			}
			if (pointIdxRadiusSearch.size() < 1) continue;
			Eigen::RowVector3d featureMeansXY =
			    traindataXY.colwise().mean();
			Eigen::MatrixXd evecsXY, evecsXYZ;
			Eigen::VectorXd evalsXY, evalsXYZ;
			this->_ComputeEigenVectorsValues(traindataXY, evecsXY,
							 evalsXY);
			this->_ComputeEigenVectorsValues(traindataXYZ, evecsXYZ,
							 evalsXYZ);
			double lambda1 = evalsXY(2);
			double lambda2 = evalsXY(1);
			double lambda3 = evalsXY(0);
			if (lambda1 > 0 && lambda2 >= 0 && lambda3 >= 0 &&
			    evalsXYZ(2) > 0 && evalsXYZ(1) >= 0 &&
			    evalsXYZ(0) >= 0 &&
			    (lambda2 / lambda1) < threshold) {
				initial_kps.push_back(i);
				initial_evsXY.push_back(
				    bsc::Vector2f(lambda1, lambda2));
				initial_evsXYZ.push_back(bsc::Vector3f(
				    evalsXYZ(2), evalsXYZ(1), evalsXYZ(0)));
				initial_e1V.push_back(
				    bsc::Vector3f(evecsXY(0, 2), evecsXY(1, 2),
						  evecsXY(2, 2)));
				init_kps_pushed++;
				kp_aloc_id[i] = 1;
			}
		}
	}

	// KP second stage
	int kps_left = initial_kps.size();
	while (kps_left > 0) {
		// Find max curvature point
		double max_curvature = 0;
		int max_curv_kp_id = 0;
		for (int i = 0; i < initial_kps.size(); i++) {
			if (kp_aloc_id[initial_kps[i]] != 1) continue;
			double curvature =
			    initial_evsXYZ[i].z /
			    (initial_evsXYZ[i].x + initial_evsXYZ[i].y +
			     initial_evsXYZ[i].z);
			if (curvature > max_curvature) {
				max_curvature = curvature;
				max_curv_kp_id = i;
			}
		}
		if (max_curvature == 0) {
			// cout << "No more good point left. Skipping " <<
			// kps_left << " keypoints\n";
			kps_left = 0;
			continue;
		}

		out_kps.push_back(initial_kps[max_curv_kp_id]);
		out_X.push_back(initial_e1V[max_curv_kp_id]);
		kp_aloc_id[initial_kps[max_curv_kp_id]] = 0;
		kps_left--;

		// Remove its neighbours
		std::vector< int > pointIdxRadiusSearch;
		std::vector< float > pointRadiusSquaredDistance;
		if (kdtree->radiusSearch(
			in_cloud->points[initial_kps[max_curv_kp_id]],
			search_radius, pointIdxRadiusSearch,
			pointRadiusSquaredDistance) > 0) {
			for (size_t j = 0; j < pointIdxRadiusSearch.size();
			     ++j) {
				if (kp_aloc_id[pointIdxRadiusSearch[j]] == 1)
					kps_left--;
				kp_aloc_id[pointIdxRadiusSearch[j]] = 0;
			}
		}
	}
	delete[] kp_aloc_id;
	if (bscDEBUG)
		std::cout << "Detected Keypoints: " << out_kps.size()
			  << std::endl;
}

void BSC::DrawKeypoints(pcl::PointCloud< pcl::PointXYZ >::Ptr in_cloud,
			std::vector< int >& kps,
			pcl::PointCloud< pcl::PointXYZRGB >::Ptr out_cloud) {
	if (bscDEBUG) std::cout << "Drawing Keypoints ... ";
	out_cloud->points.resize(in_cloud->points.size());
	for (size_t i = 0; i < in_cloud->points.size(); i++) {
		out_cloud->points[i].x = in_cloud->points[i].x;
		out_cloud->points[i].y = in_cloud->points[i].y;
		out_cloud->points[i].z = in_cloud->points[i].z;
		out_cloud->points[i].r = 255;
		out_cloud->points[i].g = 255;
		out_cloud->points[i].b = 255;
	}
	for (size_t i = 0; i < kps.size(); i++) {
		out_cloud->points[kps[i]].r = 0;
		out_cloud->points[kps[i]].g = 255;
		out_cloud->points[kps[i]].b = 0;
	}
	out_cloud->width = 1;
	out_cloud->height = out_cloud->points.size();
	if (bscDEBUG) std::cout << "Completed" << std::endl;
}

void BSC::ComputeDescriptors(pcl::PointCloud< pcl::PointXYZ >::Ptr in_cloud,
			     pcl::KdTreeFLANN< pcl::PointXYZ >::Ptr kdtree,
			     std::vector< int >& in_kps,
			     std::vector< bsc::Vector3f >& in_X, float radius,
			     std::vector< std::vector< bool > >& out_descs,
			     int h) {
	if (bscDEBUG) std::cout << "Computing Descriptors" << std::endl;
	for (size_t i = 0; i < in_kps.size(); i++) {
		std::vector< bool > desc;
		this->ComputeSingleDescriptor(in_cloud, kdtree, in_X[i], radius,
					      in_cloud->points[in_kps[i]], desc,
					      h);
		out_descs.push_back(desc);
	}
	if (bscDEBUG)
		std::cout << "Computed Descriptors: " << out_descs.size()
			  << std::endl;
}

void BSC::ConvertToCVdescriptors(std::vector< std::vector< bool > >& descs,
				 cv::Mat& out_desc) {
	int desc_length = m_num_pairs * 2 * 3 / 8;
	out_desc = cv::Mat(descs.size(), desc_length, CV_8UC1);
	for (size_t i = 0; i < descs.size(); i++) {
		for (size_t j = 0; j < desc_length; j++) {
			std::stringstream bss;
			for (size_t k = 0; k < 8; k++) {
				bss << int(descs[i][j * 8 + k]);
			}
			std::string bit_string = bss.str();
			std::bitset< 8 > b(bit_string);
			unsigned char c = (b.to_ulong() & 0xFF);
			out_desc.at< uchar >(i, j) = c;
		}
	}
}

void BSC::ComputeSingleDescriptor(
    pcl::PointCloud< pcl::PointXYZ >::Ptr in_cloud,
    pcl::KdTreeFLANN< pcl::PointXYZ >::Ptr kdtree, bsc::Vector3f& in_X,
    float radius, pcl::PointXYZ centerPt, std::vector< bool >& out_descs,
    int h) {
	out_descs.clear();
	// Create temporary point in_cloud
	pcl::PointCloud< pcl::PointXYZ >::Ptr local_cloud(
	    new pcl::PointCloud< pcl::PointXYZ >);
	pcl::PointCloud< pcl::PointXYZ >::Ptr trans_cloud(
	    new pcl::PointCloud< pcl::PointXYZ >);
	std::vector< int > pointIdxRadiusSearch;
	std::vector< float > pointRadiusSquaredDistance;
	if (kdtree->radiusSearch(centerPt, radius, pointIdxRadiusSearch,
				 pointRadiusSquaredDistance) > 0) {
		local_cloud->points.resize(pointIdxRadiusSearch.size());
		for (size_t j = 0; j < pointIdxRadiusSearch.size(); ++j) {
			local_cloud->points[j].x =
			    in_cloud->points[pointIdxRadiusSearch[j]].x -
			    centerPt.x;
			local_cloud->points[j].y =
			    in_cloud->points[pointIdxRadiusSearch[j]].y -
			    centerPt.y;
			local_cloud->points[j].z =
			    in_cloud->points[pointIdxRadiusSearch[j]].z -
			    centerPt.z;
		}
		local_cloud->width = 1;
		local_cloud->height = local_cloud->points.size();
		this->_TransformCloud(local_cloud, in_X, trans_cloud);

		// Create three projections
		int num_bins = m_s * m_s;
		std::vector< std::vector< std::vector< int > > > projec_bins(
		    3, std::vector< std::vector< int > >(
			   num_bins));  // Contains the id of the point in
					// trans_cloud which lies in that bin
		for (size_t j = 0; j < trans_cloud->points.size(); j++) {
			bsc::Vector3f temp_point(
			    ((trans_cloud->points[j].x + radius) * m_s) /
				(2 * radius),
			    ((trans_cloud->points[j].y + radius) * m_s) /
				(2 * radius),
			    ((trans_cloud->points[j].z + radius) * m_s) /
				(2 * radius));
			projec_bins[0]
				   [int(temp_point.y) * m_s + int(temp_point.x)]
				       .push_back(j);
			projec_bins[1]
				   [int(temp_point.z) * m_s + int(temp_point.y)]
				       .push_back(j);
			projec_bins[2]
				   [int(temp_point.x) * m_s + int(temp_point.z)]
				       .push_back(j);
		}

		// Get weighted density and distance feature for each point in a
		// bin and accumulate it for the features of the bins
		std::vector< std::vector< std::vector< float > > > bin_features(
		    3, std::vector< std::vector< float > >(
			   num_bins, std::vector< float >(2)));
		for (size_t j = 0; j < 3; j++) {
			double total_density = 0;
			double total_distance = 0;
			for (size_t k = 0; k < num_bins; k++) {
				double bin_density = 0;
				double bin_distance = 0;
				for (size_t l = 0; l < projec_bins[j][k].size();
				     l++) {
					double temp_density =
					    this->_GetWeightedDensity();
					double temp_distance =
					    this->_GetWeightedDistance(
						trans_cloud->points[l], j);
					bin_density += temp_density;
					bin_distance += temp_distance;
					total_density += temp_density;
					total_distance += temp_distance;
				}
				bin_features[j][k][0] = bin_density;
				bin_features[j][k][1] = bin_distance;
			}
			for (size_t k = 0; k < num_bins; k++) {
				bin_features[j][k][0] =
				    bin_features[j][k][0] / total_density;
				bin_features[j][k][1] =
				    bin_features[j][k][1] / total_distance;
			}
		}

		// Difference test to compute the descriptor
		std::vector< float > sigma_density(3);
		std::vector< float > sigma_distance(3);
		std::vector< float > mean_density(3);
		std::vector< float > mean_distance(3);
		for (size_t j = 0; j < 3; j++) {
			double temp_mean_density = 0;
			double temp_mean_distance = 0;
			for (size_t k = 0; k < m_num_pairs; k++) {
				temp_mean_density +=
				    bin_features[j][m_pairs[k].first][0] -
				    bin_features[j][m_pairs[k].second][0];
				temp_mean_distance +=
				    bin_features[j][m_pairs[k].first][1] -
				    bin_features[j][m_pairs[k].second][1];
			}
			mean_density[j] = temp_mean_density / m_num_pairs;
			mean_distance[j] = temp_mean_distance / m_num_pairs;
		}
		for (size_t j = 0; j < 3; j++) {
			double temp_sigma_density = 0;
			double temp_sigma_distance = 0;
			for (size_t k = 0; k < m_num_pairs; k++) {
				temp_sigma_density += std::pow(
				    bin_features[j][m_pairs[k].first][0] -
					bin_features[j][m_pairs[k].second][0] -
					mean_density[j],
				    2);
				temp_sigma_distance += std::pow(
				    bin_features[j][m_pairs[k].first][1] -
					bin_features[j][m_pairs[k].second][1] -
					mean_distance[j],
				    2);
			}
			sigma_density[j] =
			    std::sqrt(temp_sigma_density / (m_num_pairs - 1));
			sigma_distance[j] =
			    std::sqrt(temp_sigma_distance / (m_num_pairs - 1));
		}
		for (size_t j = 0; j < 3; j++) {
			for (size_t k = 0; k < m_num_pairs; k++) {
				bool test_pass_density = false;
				bool test_pass_distance = false;
				if (std::fabs(
					bin_features[j][m_pairs[k].first][0] -
					bin_features[j][m_pairs[k].second][0]) >
				    sigma_density[j])
					test_pass_density = true;
				out_descs.push_back(test_pass_density);
				if (std::fabs(
					bin_features[j][m_pairs[k].first][1] -
					bin_features[j][m_pairs[k].second][1]) >
				    sigma_distance[j])
					test_pass_distance = true;
				out_descs.push_back(test_pass_distance);
			}
		}
	}
}

void BSC::CompareMatchesWithGT(std::vector< cv::DMatch >& matches,
			       pcl::PointCloud< pcl::PointXYZ >::Ptr pc1,
			       pcl::PointCloud< pcl::PointXYZ >::Ptr pc2,
			       std::vector< int >& kps_1,
			       std::vector< int >& kps_2,
			       std::vector< cv::DMatch >& inliers) {
	if (bscDEBUG) std::cout << "Comparing Matches with GT" << std::endl;
	inliers.clear();
	float distance_threshold = 1.0;
	Eigen::Matrix4d gtT2C, gtC2T;
	gtT2C << -0.551, -0.834, 0.012, -6.356, 0.834, -0.551, 0.011, -4.097,
	    -0.003, 0.016, 1.000, 0.978, -0.000, -0.000, -0.000, 1.000;
	// gtC2T = gtT2C.inverse();

	for (int i = 0; i < matches.size(); i++) {
		Eigen::Vector4d aligned(
		    pc2->points[kps_2[matches[i].trainIdx]].x,
		    pc2->points[kps_2[matches[i].trainIdx]].y,
		    pc2->points[kps_2[matches[i].trainIdx]].z, 1);
		Eigen::Vector4d query(pc1->points[kps_1[matches[i].queryIdx]].x,
				      pc1->points[kps_1[matches[i].queryIdx]].y,
				      pc1->points[kps_1[matches[i].queryIdx]].z,
				      1);

		Eigen::Vector4d alq = gtT2C * aligned;
		alq(0) = alq(0) / alq(3);
		alq(1) = alq(1) / alq(3);
		alq(2) = alq(2) / alq(3);
		double distance =
		    sqrt(pow(alq(0) - query(0), 2) + pow(alq(1) - query(1), 2) +
			 pow(alq(2) - query(2), 2));
		if (distance < distance_threshold)
			inliers.push_back(matches[i]);
	}
	if (bscDEBUG)
		std::cout << "Found GT Inlier Matches: " << inliers.size()
			  << std::endl;
}

void BSC::InvertXaxes(std::vector< bsc::Vector3f >& in_X,
		      std::vector< bsc::Vector3f >& out_X) {
	if (bscDEBUG) std::cout << "Inverting X axis .. ";
	out_X.clear();
	for (size_t i = 0; i < in_X.size(); i++) {
		bsc::Vector3f temp(-in_X[i].x, -in_X[i].y, -in_X[i].z);
		out_X.push_back(temp);
	}
	if (bscDEBUG) std::cout << "Completed" << std::endl;
}

void BSC::MatchDescriptors(std::vector< std::vector< bool > >& desc_q,
			   std::vector< std::vector< bool > >& desc_q2,
			   std::vector< std::vector< bool > >& desc_t,
			   float threshold,
			   std::vector< bsc::DMatch >& out_matches) {
	if (bscDEBUG) std::cout << "Matching Descriptors" << std::endl;
	out_matches.clear();
	float max_cost = m_num_pairs * 3 * 2;
	for (size_t i = 0; i < desc_q.size(); i++) {
		int min_cost = max_cost;
		int min_id = 0;
		for (size_t j = 0; j < desc_t.size(); j++) {
			int cost1 =
			    this->_HammingDistance(desc_q[i], desc_t[j]);
			int cost2 =
			    this->_HammingDistance(desc_q2[i], desc_t[j]);
			float this_cost = std::min(cost1, cost2);
			if (this_cost < min_cost) {
				min_cost = this_cost;
				min_id = j;
			}
		}
		if (float(min_cost) / max_cost < threshold) {
			bsc::DMatch this_match(i, min_id, min_cost);
			out_matches.push_back(this_match);
		}
	}
	if (bscDEBUG)
		std::cout << "Found matches: " << out_matches.size()
			  << std::endl;
}

void BSC::MatchCVDescriptors(cv::Mat& desc_q, cv::Mat& desc_q2, cv::Mat& desc_t,
			     std::vector< cv::DMatch >& out_matches) {
	if (bscDEBUG) std::cout << "Matching CV descriptors" << std::endl;
	out_matches.clear();
	std::vector< cv::DMatch > dup_matches(desc_q.rows);
	std::vector< int > have_match(desc_q.rows);
	for (int i = 0; i < have_match.size(); i++) have_match[i] = 0;
	int first_select = 0, second_select = 0;
	std::vector< cv::DMatch > dmatches;
	cv::BFMatcher matcher(cv::NORM_HAMMING, true);
	matcher.match(desc_q, desc_t, dmatches);
	for (int i = 0; i < dmatches.size(); i++) {
		dup_matches[dmatches[i].queryIdx] = dmatches[i];
		have_match[dmatches[i].queryIdx] = 1;
		first_select++;
	}
	dmatches.clear();
	matcher.match(desc_q2, desc_t, dmatches);
	for (int i = 0; i < dmatches.size(); i++) {
		if (have_match[dmatches[i].queryIdx] == 1) {
			if (dup_matches[dmatches[i].queryIdx].distance >
			    dmatches[i].distance) {
				dup_matches[dmatches[i].queryIdx] = dmatches[i];
				second_select++;
			}
		} else {
			dup_matches[dmatches[i].queryIdx] = dmatches[i];
			have_match[dmatches[i].queryIdx] = 1;
		}
	}
	for (int i = 0; i < dup_matches.size(); i++) {
		if (have_match[i] == 1) out_matches.push_back(dup_matches[i]);
	}
	if (bscDEBUG)
		std::cout << "Found matches: " << out_matches.size() << " "
			  << first_select << " " << second_select << std::endl;
}

void BSC::FindTransformation(pcl::PointCloud< pcl::PointXYZ >::Ptr cloud_q,
			     pcl::PointCloud< pcl::PointXYZ >::Ptr cloud_t,
			     std::vector< int >& kps_q,
			     std::vector< int >& kps_t,
			     std::vector< cv::DMatch >& inliers,
			     Eigen::Matrix4f& out_trans) {
	pcl::registration::TransformationEstimationSVD< pcl::PointXYZ,
							pcl::PointXYZ >
	    trans_est;
	pcl::Correspondences correspondences;
	for (int i = 0; i < inliers.size(); i++) {
		pcl::Correspondence temp;
		temp.index_query = kps_q[inliers[i].queryIdx];
		temp.index_match = kps_t[inliers[i].trainIdx];
		temp.distance = std::sqrt(
		    std::pow(cloud_q->points[kps_q[inliers[i].queryIdx]].x -
				 cloud_t->points[kps_t[inliers[i].trainIdx]].x,
			     2) +
		    std::pow(cloud_q->points[kps_q[inliers[i].queryIdx]].y -
				 cloud_t->points[kps_t[inliers[i].trainIdx]].y,
			     2) +
		    std::pow(cloud_q->points[kps_q[inliers[i].queryIdx]].z -
				 cloud_t->points[kps_t[inliers[i].trainIdx]].z,
			     2));
		correspondences.push_back(temp);
	}
	trans_est.estimateRigidTransformation(*cloud_q, *cloud_t,
					      correspondences, out_trans);
}

void BSC::DrawRegistration(pcl::PointCloud< pcl::PointXYZ >::Ptr cloud_q_al,
			   pcl::PointCloud< pcl::PointXYZ >::Ptr cloud_t,
			   pcl::PointCloud< pcl::PointXYZRGB >::Ptr out_cloud) {
	if (bscDEBUG) std::cout << "Drawing Registration ... ";
	out_cloud->points.resize(cloud_t->points.size() +
				 cloud_q_al->points.size());
	int counter = 0;
	for (size_t i = 0; i < cloud_q_al->points.size(); i++, counter++) {
		out_cloud->points[counter].x = cloud_q_al->points[i].x;
		out_cloud->points[counter].y = cloud_q_al->points[i].y;
		out_cloud->points[counter].z = cloud_q_al->points[i].z;
		out_cloud->points[counter].r = 255;
		out_cloud->points[counter].g = 0;
		out_cloud->points[counter].b = 0;
	}
	for (size_t i = 0; i < cloud_t->points.size(); i++, counter++) {
		out_cloud->points[counter].x = cloud_t->points[i].x;
		out_cloud->points[counter].y = cloud_t->points[i].y;
		out_cloud->points[counter].z = cloud_t->points[i].z;
		out_cloud->points[counter].r = 255;
		out_cloud->points[counter].g = 255;
		out_cloud->points[counter].b = 0;
	}
	out_cloud->width = 1;
	out_cloud->height = out_cloud->points.size();
	if (bscDEBUG) std::cout << "Completed" << std::endl;
}

void BSC::GeometricConsistantInliers(std::vector< cv::DMatch >& matches,
				     pcl::PointCloud< pcl::PointXYZ >::Ptr pc1,
				     pcl::PointCloud< pcl::PointXYZ >::Ptr pc2,
				     std::vector< int >& kps_1,
				     std::vector< int >& kps_2, float threshold,
				     std::vector< cv::DMatch >& inliers) {
	if (bscDEBUG) std::cout << "Filter based on Geometric Consistency ... ";
	inliers.clear();
	std::srand((unsigned)std::time(NULL));
	std::vector< std::vector< cv::DMatch > > cluster;
	std::vector< cv::DMatch > left_matches;
	left_matches = matches;
	int num_matches = left_matches.size();
	int largest_cluster_size = 0;
	int largest_cluster_id = 0;
	int counter = 0;
	int while_runner = 0;
	int trials = 7000;
	while (left_matches.size() > 6 &&
	       largest_cluster_size < left_matches.size() &&
	       while_runner < matches.size()) {
		pcl::PointXYZ centroid1, centroid2;
		bool found_triangle = false;
		std::vector< cv::DMatch > this_cluster;
		std::vector< cv::DMatch > this_left;
		int id1 = 0, id2 = 0, id3 = 0;
		while (!found_triangle && trials > 0) {
			id1 = rand() % num_matches;
			id2 = rand() % num_matches;
			id3 = rand() % num_matches;
			if (id1 == id2 || id2 == id3 || id3 == id1) continue;
			float d21 = this->_Distance(
			    pc1->points[kps_1[left_matches[id2].queryIdx]],
			    pc1->points[kps_1[left_matches[id1].queryIdx]]);
			float d32 = this->_Distance(
			    pc1->points[kps_1[left_matches[id3].queryIdx]],
			    pc1->points[kps_1[left_matches[id2].queryIdx]]);
			float d31 = this->_Distance(
			    pc1->points[kps_1[left_matches[id3].queryIdx]],
			    pc1->points[kps_1[left_matches[id1].queryIdx]]);

			float cd21 = this->_Distance(
			    pc2->points[kps_2[left_matches[id2].trainIdx]],
			    pc2->points[kps_2[left_matches[id1].trainIdx]]);
			float cd32 = this->_Distance(
			    pc2->points[kps_2[left_matches[id3].trainIdx]],
			    pc2->points[kps_2[left_matches[id2].trainIdx]]);
			float cd31 = this->_Distance(
			    pc2->points[kps_2[left_matches[id3].trainIdx]],
			    pc2->points[kps_2[left_matches[id1].trainIdx]]);

			if (std::fabs(d21 - cd21) < threshold &&
			    std::fabs(d32 - cd32) < threshold &&
			    std::fabs(d31 - cd31) < threshold) {
				found_triangle = true;
				centroid1.x =
				    (pc1->points
					 [kps_1[left_matches[id1].queryIdx]]
					     .x +
				     pc1->points
					 [kps_1[left_matches[id2].queryIdx]]
					     .x +
				     pc1->points
					 [kps_1[left_matches[id3].queryIdx]]
					     .x) /
				    3.0;
				centroid1.y =
				    (pc1->points
					 [kps_1[left_matches[id1].queryIdx]]
					     .y +
				     pc1->points
					 [kps_1[left_matches[id2].queryIdx]]
					     .y +
				     pc1->points
					 [kps_1[left_matches[id3].queryIdx]]
					     .y) /
				    3.0;
				centroid1.z =
				    (pc1->points
					 [kps_1[left_matches[id1].queryIdx]]
					     .z +
				     pc1->points
					 [kps_1[left_matches[id2].queryIdx]]
					     .z +
				     pc1->points
					 [kps_1[left_matches[id3].queryIdx]]
					     .z) /
				    3.0;

				centroid2.x =
				    (pc2->points
					 [kps_2[left_matches[id1].trainIdx]]
					     .x +
				     pc2->points
					 [kps_2[left_matches[id2].trainIdx]]
					     .x +
				     pc2->points
					 [kps_2[left_matches[id3].trainIdx]]
					     .x) /
				    3.0;
				centroid2.y =
				    (pc2->points
					 [kps_2[left_matches[id1].trainIdx]]
					     .y +
				     pc2->points
					 [kps_2[left_matches[id2].trainIdx]]
					     .y +
				     pc2->points
					 [kps_2[left_matches[id3].trainIdx]]
					     .y) /
				    3.0;
				centroid2.z =
				    (pc2->points
					 [kps_2[left_matches[id1].trainIdx]]
					     .z +
				     pc2->points
					 [kps_2[left_matches[id2].trainIdx]]
					     .z +
				     pc2->points
					 [kps_2[left_matches[id3].trainIdx]]
					     .z) /
				    3.0;
				this_cluster.push_back(left_matches[id1]);
				this_cluster.push_back(left_matches[id2]);
				this_cluster.push_back(left_matches[id3]);
			}
			trials--;
		}
		if (found_triangle) {
			for (int i = 0; i < num_matches; i++) {
				if (i == id1 || i == id2 || i == id3 ||
				    id1 == id2)
					continue;
				float dc4 = this->_Distance(
				    centroid1,
				    pc1->points
					[kps_1[left_matches[i].queryIdx]]);
				float cdc4 = this->_Distance(
				    centroid2,
				    pc2->points
					[kps_2[left_matches[i].trainIdx]]);
				if (std::fabs(dc4 - cdc4) < threshold) {
					this_cluster.push_back(left_matches[i]);
				} else {
					this_left.push_back(left_matches[i]);
				}
			}
			cluster.push_back(this_cluster);
			left_matches = this_left;
			num_matches = this_left.size();
			if (largest_cluster_size < this_cluster.size()) {
				largest_cluster_id = counter;
				largest_cluster_size = this_cluster.size();
			}
			counter++;
		}
		while_runner++;
	}
	inliers = cluster[largest_cluster_id];
	if (bscDEBUG)
		std::cout << "Filtered. Found inliers " << inliers.size()
			  << std::endl;
}

void BSC::RansacInliers(std::vector< cv::DMatch >& matches,
			pcl::PointCloud< pcl::PointXYZ >::Ptr pc1,
			pcl::PointCloud< pcl::PointXYZ >::Ptr pc2,
			std::vector< int >& kps_1, std::vector< int >& kps_2,
			float threshold, std::vector< cv::DMatch >& inliers) {
	if (bscDEBUG) std::cout << "Filter based on RANSAC ... \n";
	inliers.clear();
	std::srand((unsigned)std::time(NULL));
	int max_inliers = 0;
	std::vector< cv::DMatch > final_inliers;
	int snp_points = 3;
	int num_ransac = 50000;
#pragma omp parallel for num_threads(7) shared( \
    final_inliers, max_inliers, matches, pc1, pc2, kps_1, kps_2, threshold)
	for (int i = 0; i < num_ransac; i++) {
		Eigen::Matrix4f this_trans;
		std::vector< cv::DMatch > this_matches;
		int pt_c = 0;
		int pt_id[3] = {-1};
		while (pt_c < snp_points) {
			int id = rand() % matches.size();
			if (id != pt_id[0] && id != pt_id[1] &&
			    id != pt_id[2]) {
				this_matches.push_back(matches[id]);
				pt_c++;
			}
		}
		this->FindTransformation(pc1, pc2, kps_1, kps_2, this_matches,
					 this_trans);
		std::vector< cv::DMatch > run_inliers;
		this->_FindInliers(matches, pc1, pc2, kps_1, kps_2, this_trans,
				   threshold, run_inliers);
		if (run_inliers.size() > max_inliers) {
			max_inliers = run_inliers.size();
			final_inliers = run_inliers;
			std::cout
			    << "Interim num-inliers: " << run_inliers.size()
			    << " in " << i << std::endl;
		}
	}
	inliers = final_inliers;
	if (bscDEBUG)
		std::cout << "Found Inliers: " << inliers.size() << std::endl;
}

void BSC::WriteKpX(std::vector< int >& kps, std::vector< bsc::Vector3f >& X,
		   std::string filename) {
	if (bscDEBUG) std::cout << "Writing KpX ... ";
	Json::Value mainObj;
	for (int i = 0; i < kps.size(); i++) {
		Json::Value obj;
		obj["k"].append(kps[i]);
		obj["X"].append(X[i].x);
		obj["X"].append(X[i].y);
		obj["X"].append(X[i].z);
		mainObj["objects"].append(obj);
	}
	Json::FastWriter fast;
	std::string sFast = fast.write(mainObj);
	std::ofstream imgfile;
	imgfile.open(filename.c_str());
	imgfile << sFast;
	imgfile.close();
	if (bscDEBUG) std::cout << "Done\n";
}

void BSC::ReadKpX(std::vector< int >& kps, std::vector< bsc::Vector3f >& X,
		  std::string filename) {
	if (bscDEBUG) std::cout << "Reading KpX ... ";
	kps.clear();
	X.clear();
	std::ifstream ifs(filename.c_str());
	Json::Value mainObj;
	ifs >> mainObj;
	int num_objs = mainObj["objects"].size();
	if (num_objs <= 0) return;
	int desc_len = mainObj["objects"][0]["D"].size();
	for (int i = 0; i < num_objs; i++) {
		X.push_back(
		    bsc::Vector3f(mainObj["objects"][i]["X"][0].asFloat(),
				  mainObj["objects"][i]["X"][1].asFloat(),
				  mainObj["objects"][i]["X"][2].asFloat()));
		kps.push_back(mainObj["objects"][i]["k"][0].asInt());
	}
	if (bscDEBUG) std::cout << "Loaded: " << kps.size() << std::endl;
}

void BSC::_ComputeEigenVectorsValues(Eigen::MatrixX3d& traindata,
				     Eigen::MatrixXd& evecs,
				     Eigen::VectorXd& evels) {
	Eigen::RowVector3d featureMeans = traindata.colwise().mean();
	Eigen::MatrixX3d centered = traindata.rowwise() - featureMeans;
	Eigen::MatrixXd cov = centered.adjoint() * centered;
	cov = cov / (traindata.rows() - 1);
	Eigen::SelfAdjointEigenSolver< Eigen::MatrixXd > eig(cov);
	evels = eig.eigenvalues();  // / eig.eigenvalues().sum();
	evecs = eig.eigenvectors();
}

void BSC::_TransformCloud(pcl::PointCloud< pcl::PointXYZ >::Ptr in_cloud,
			  bsc::Vector3f& in_X,
			  pcl::PointCloud< pcl::PointXYZ >::Ptr out_cloud) {
	out_cloud->points.resize(in_cloud->points.size());
	Eigen::Matrix3f matrix;
	matrix << in_X.x, in_X.y, 0.0f, -in_X.y, in_X.x, 0.0f, 0.0f, 0.0f, 1.0f;
	for (size_t i = 0; i < in_cloud->points.size(); i++) {
		Eigen::Vector3f transformed_point(in_cloud->points[i].x,
						  in_cloud->points[i].y,
						  in_cloud->points[i].z);
		transformed_point = matrix * transformed_point;
		out_cloud->points[i].x = transformed_point(0);
		out_cloud->points[i].y = transformed_point(1);
		out_cloud->points[i].z = transformed_point(2);
	}
	out_cloud->width = 1;
	out_cloud->height = out_cloud->points.size();
}

double BSC::_GetWeightedDensity() { return 1.0; }

double BSC::_GetWeightedDistance(pcl::PointXYZ pt, int plane) {
	if (plane == 0) return std::fabs(pt.z);
	if (plane == 1) return std::fabs(pt.x);
	if (plane == 2) return std::fabs(pt.y);
}

int BSC::_HammingDistance(const std::vector< bool >& desc1,
			  const std::vector< bool >& desc2) {
	int mismatch = 0;
	for (size_t i = 0; i < desc1.size(); i++) {
		if (desc1[i] != desc2[i]) mismatch++;
	}
	return mismatch;
}

void BSC::_FindInliers(std::vector< cv::DMatch >& matches,
		       pcl::PointCloud< pcl::PointXYZ >::Ptr pc1,
		       pcl::PointCloud< pcl::PointXYZ >::Ptr pc2,
		       std::vector< int >& kps_1, std::vector< int >& kps_2,
		       Eigen::Matrix4f& trans, float threshold,
		       std::vector< cv::DMatch >& inliers) {
	// if(bscDEBUG) std::cout << "Finding Inliers based on RANSAC" <<
	// std::endl;
	inliers.clear();
	for (int i = 0; i < matches.size(); i++) {
		Eigen::Vector4f aligned(
		    pc2->points[kps_2[matches[i].trainIdx]].x,
		    pc2->points[kps_2[matches[i].trainIdx]].y,
		    pc2->points[kps_2[matches[i].trainIdx]].z, 1);
		Eigen::Vector4f query(pc1->points[kps_1[matches[i].queryIdx]].x,
				      pc1->points[kps_1[matches[i].queryIdx]].y,
				      pc1->points[kps_1[matches[i].queryIdx]].z,
				      1);

		Eigen::Vector4f alq = trans * query;
		alq(0) = alq(0) / alq(3);
		alq(1) = alq(1) / alq(3);
		alq(2) = alq(2) / alq(3);
		double distance = sqrt(pow(alq(0) - aligned(0), 2) +
				       pow(alq(1) - aligned(1), 2) +
				       pow(alq(2) - aligned(2), 2));
		if (distance < threshold) inliers.push_back(matches[i]);
	}
	// if(bscDEBUG) std::cout << "Found Inlier Matches: " << inliers.size()
	// << std::endl;
}

float BSC::_Distance(pcl::PointXYZ pt1, pcl::PointXYZ pt2) {
	float distance =
	    std::sqrt(std::pow(pt1.x - pt2.x, 2) + std::pow(pt1.y - pt2.y, 2) +
		      std::pow(pt1.z - pt2.z, 2));
	return distance;
}

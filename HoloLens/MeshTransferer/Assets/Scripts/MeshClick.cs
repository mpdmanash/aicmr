﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.IO;
using HoloToolkit.Unity.SpatialMapping;
using System.Text;


#if WINDOWS_UWP
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Storage.Streams;
using System.Threading.Tasks;
using Windows.Networking.Sockets;
using Windows.Storage;
using Windows.Data.Xml.Dom;
#endif

public class MeshClick
{
    public bool write_local = false; // Flag to switch between local storage vs TCP transfer
    private List<Vector3> vertices = new List<Vector3>();
    private List<Vector3> normals = new List<Vector3>();
    private Vector3 exportPosition;
    private string host_ip = "10.42.0.1";

#if WINDOWS_UWP
    Windows.Networking.Sockets.StreamSocket socket;
    Windows.Networking.Sockets.StreamSocket secondsocket;
    IOutputStream secondoutstream;
    DataReader dr;
#endif

    public void exportMesh(ref bool status, bool firstClick)
    {
        Debug.Log("Export Mesh Called");
        saveCamPosition();
        // If it the first click for this app session, 
        // Then transmit the mesh to server
        // Else transmit only raycasted point for selection
        if (firstClick) grab();
        else placeSphere();
        status = true;
    }

#if UNITY_EDITOR
    private void saveCamPosition() { }
#elif WINDOWS_UWP
    private async void saveCamPosition()
    {
        String localDate = DateTime.Now.Ticks.ToString();

        Vector3 headPosition = Camera.main.transform.position;
        Quaternion headOrientation = Camera.main.transform.rotation;
        // Write the data to file
        Stream textFileForWrite_stream2 = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(localDate + ".txt", CreationCollisionOption.ReplaceExisting);
        String pos_data = ("Position: " + headPosition.x.ToString() + " " + headPosition.y.ToString() + " " + headPosition.z.ToString() + Environment.NewLine +
                          "Orientation: " + headOrientation.x.ToString() + " " +
                                            headOrientation.y.ToString() + " " +
                                            headOrientation.z.ToString() + " " +
                                            headOrientation.w.ToString() + " " + Environment.NewLine);
        textFileForWrite_stream2.Seek(0, SeekOrigin.End);
        byte[] pos_ba = Encoding.ASCII.GetBytes(pos_data);
        await textFileForWrite_stream2.WriteAsync(pos_ba, 0, pos_ba.Length);
        await textFileForWrite_stream2.FlushAsync();
        textFileForWrite_stream2.Dispose();
        Debug.Log("Writing position data: " + pos_data);
    }
#endif

#if UNITY_EDITOR
    private void grab() { }
#elif WINDOWS_UWP
    private void grab()
    {
        exportPosition = Camera.main.transform.position;
        List<MeshFilter> meshFilters = SpatialMappingManager.Instance.GetMeshFilters();
        foreach (MeshFilter filter in meshFilters)
        {
            // Since this is amortized across frames, the filter can be destroyed by the time
            // we get here.
            if (filter == null)
            {
                continue;
            }

            Mesh mesh = filter.sharedMesh;
            MeshRenderer renderer = filter.GetComponent<MeshRenderer>();

            // The mesh renderer bounds are in world space.
            // If the mesh is null there is nothing to process
            // If the renderer is null we can't get the renderer bounds
            // If the renderer's bounds aren't contained inside of the current
            // bounds from the bounds queue there is no reason to process
            // If any of the above conditions are met, then we should go to the next meshfilter. 
            if (mesh == null || renderer == null)
            {
                // We don't need to do anything to this mesh, move to the next one.
                continue;
            }

            // Remove vertices from any mesh that intersects with the bounds.
            Vector3[] verts = mesh.vertices;
            //Vector3[] norms = mesh.normals;

            // Find which mesh vertices are within the bounds.
            for (int i = 0; i < verts.Length; ++i)
            {
                vertices.Add(filter.transform.TransformPoint(verts[i]));
                //normals.Add(filter.transform.TransformPoint(norms[i]));
            }
        }
        Debug.Log("Completed grabbing data");
        Task t = export();
        Task.WaitAll(t);
        Debug.Log("Waited");

        //Start while to receive comms
        //receiveComms();
    }
#endif

#if UNITY_EDITOR
    private void export() { }
#elif WINDOWS_UWP
    private async Task export()
    {
        try
        {
            Task[] pt = new Task[vertices.Count + 2];
            int m = 0;
            if (!write_local)
            {
                socket = new Windows.Networking.Sockets.StreamSocket();
                Windows.Networking.HostName serverHost = new Windows.Networking.HostName(host_ip); //192.168.43.199 169.254.80.80
                string serverPort = "444";
                await socket.ConnectAsync(serverHost, serverPort);                
                var outStram = socket.OutputStream;
                dr = new DataReader(socket.InputStream);
                dr.InputStreamOptions = InputStreamOptions.Partial;
                receiveComms();
                Debug.Log("#vertices: " + vertices.Count.ToString());
                // Format the data as PCL point cloud pcd file
                String preamble = ("# .PCD v.7 - Point Cloud Data file format" + Environment.NewLine +
                                  "VERSION .7" + Environment.NewLine +
                                  "FIELDS x y z" + Environment.NewLine +
                                  "SIZE 4 4 4" + Environment.NewLine +
                                  "TYPE F F F" + Environment.NewLine +
                                  "COUNT 1 1 1" + Environment.NewLine +
                                  "WIDTH " + (vertices.Count).ToString() + Environment.NewLine +
                                  "HEIGHT 1" + Environment.NewLine +
                                  "VIEWPOINT 0 0 0 1 0 0 0" + Environment.NewLine +
                                  "POINTS " + (vertices.Count).ToString() + Environment.NewLine +
                                  "DATA ascii" + Environment.NewLine);
                byte[] pba = Encoding.ASCII.GetBytes(preamble);
                IBuffer pbf = pba.AsBuffer();
                pt[m++] = outStram.WriteAsync(pbf).AsTask();
                foreach (Vector3 b in vertices)
                {
                    string request = b.z.ToString() + //
                                     " " + b.x.ToString() + //
                                     " " + (-b.y).ToString() + "\n";
                    byte[] ba = Encoding.ASCII.GetBytes(request);
                    IBuffer bf = ba.AsBuffer();
                    pt[m++] = outStram.WriteAsync(bf).AsTask();
                }
                byte[] eba = Encoding.ASCII.GetBytes("Done");
                IBuffer ebf = eba.AsBuffer();
                pt[m++] = outStram.WriteAsync(ebf).AsTask();
                await Task.WhenAll(pt);
                await outStram.FlushAsync();
                outStram.Dispose();

                vertices.Clear();                
                
            }
            else {
                String localDate = DateTime.Now.Ticks.ToString();                
                Stream textFileForWrite_stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(localDate + ".pcd", CreationCollisionOption.ReplaceExisting);
                String preamble = ("# .PCD v.7 - Point Cloud Data file format" + Environment.NewLine +
                                  "VERSION .7" + Environment.NewLine +
                                  "FIELDS y x z" + Environment.NewLine +
                                  "SIZE 4 4 4" + Environment.NewLine +
                                  "TYPE F F F" + Environment.NewLine +
                                  "COUNT 1 1 1" + Environment.NewLine +
                                  "WIDTH " + (vertices.Count).ToString() + Environment.NewLine +
                                  "HEIGHT 1" + Environment.NewLine +
                                  "VIEWPOINT 0 0 0 1 0 0 0" + Environment.NewLine +
                                  "POINTS " + (vertices.Count).ToString() + Environment.NewLine +
                                  "DATA ascii" + Environment.NewLine);
                textFileForWrite_stream.Seek(0, SeekOrigin.End);
                byte[] pba = Encoding.ASCII.GetBytes(preamble);
                await textFileForWrite_stream.WriteAsync(pba, 0, pba.Length);
                foreach (Vector3 b in vertices)
                {
                    String request = b.x.ToString() + //
                                     " " + b.y.ToString() + //
                                     " " + b.z.ToString() + Environment.NewLine;
                    textFileForWrite_stream.Seek(0, SeekOrigin.End);
                    byte[] ba = Encoding.ASCII.GetBytes(request);
                    await textFileForWrite_stream.WriteAsync(ba, 0, ba.Length);
                }
                vertices.Clear();
                await textFileForWrite_stream.FlushAsync();
                textFileForWrite_stream.Dispose();

            }
            Debug.Log("Exported all mesh data");
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }
#endif

#if UNITY_EDITOR
    private void receiveComms() { }
#elif WINDOWS_UWP
    private async void receiveComms()
    {
        try
        {
            Debug.Log("Waiting for Comms");
            
            /*var stringHeader = await dr.LoadAsync(4);
            if (stringHeader == 0)
            {
                // disconnected
                return;
            }*/

            int strLength = 50;
            uint numStrBytes = await dr.LoadAsync((uint)strLength);
            string msg = dr.ReadString(numStrBytes);
            Debug.Log("This is the message received:" + msg);

            if (msg.Contains("RT"))
            {
                UnityMainThreadDispatcher.Instance().Enqueue(() => SpatialMappingManager.Instance.DrawVisualMeshes = false);                
                socket.Dispose();
                secondsocket = new Windows.Networking.Sockets.StreamSocket();
                Windows.Networking.HostName serverHost = new Windows.Networking.HostName(host_ip); //192.168.43.199 169.254.80.80
                string serverPort = "445";
                await secondsocket.ConnectAsync(serverHost, serverPort);
                dr = new DataReader(secondsocket.InputStream);
                dr.InputStreamOptions = InputStreamOptions.Partial;
                secondoutstream = secondsocket.OutputStream;
            }
            else
            {
                // Add sphere at this location
                string[] words = msg.Split(':');
                if (words.Length == 4)
                {
                    Vector3 this_position = new Vector3((float)Convert.ToDouble(words[1]), -(float)Convert.ToDouble(words[2]), (float)Convert.ToDouble(words[0]));
                    UnityMainThreadDispatcher.Instance().Enqueue(placeSphereServer(this_position)); 
                }
            }
            receiveComms();

        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
        
    }
#endif

#if UNITY_EDITOR
    private void placeSphere() { }
#elif WINDOWS_UWP
    private void placeSphere()
    {
        // Do a raycast into the world that will only hit the Spatial Mapping mesh.
        var headPosition = Camera.main.transform.position;
        var gazeDirection = Camera.main.transform.forward;

        RaycastHit hitInfo;
        if (Physics.Raycast(headPosition, gazeDirection, out hitInfo,
                    30.0f, SpatialMappingManager.Instance.LayerMask))
        {
            // Add sphere red
            GameObject new_sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            new_sphere.GetComponent<Renderer>().material.color = Color.red;
            new_sphere.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            Renderer rend = new_sphere.GetComponent<Renderer>();
            rend.material.shader = Shader.Find("Legacy Shaders/Diffuse");
            rend.material.color = Color.red;

            // Place the object
            new_sphere.transform.position = hitInfo.point;

            // Send the position over socket
            transmitPoint( hitInfo.point, secondoutstream);
        }
    }
#endif

#if UNITY_EDITOR
    private void transmitPoint(Vector3 pt) { }
#elif WINDOWS_UWP
    private async void transmitPoint(Vector3 pt, IOutputStream outstrm)
    {
        try
        {
            Debug.Log("Sending Clicked position");
            string request = pt.z.ToString() + //
                             ":" + pt.x.ToString() + //
                             ":" + (-pt.y).ToString() + ":\n";
            Debug.Log(request);
            byte[] ba = Encoding.ASCII.GetBytes(request);
            IBuffer bf = ba.AsBuffer();
            //await outstrm.WriteAsync(bf);

            var tsocket = new Windows.Networking.Sockets.StreamSocket();
            Windows.Networking.HostName serverHost = new Windows.Networking.HostName(host_ip); //192.168.43.199 169.254.80.80
            string serverPort = "446";
            await tsocket.ConnectAsync(serverHost, serverPort);
            var outStram = tsocket.OutputStream;
            
            await outStram.WriteAsync(bf).AsTask();
            await outStram.FlushAsync();
            outStram.Dispose();
            tsocket.Dispose();
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }
#endif

    public IEnumerator placeSphereServer(Vector3 position)
    {        
        // Add sphere red
        GameObject new_sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        new_sphere.GetComponent<Renderer>().material.color = Color.green;
        new_sphere.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
        Renderer rend = new_sphere.GetComponent<Renderer>();
        rend.material.shader = Shader.Find("Legacy Shaders/Diffuse");
        rend.material.color = Color.green;

        // Place the object
        new_sphere.transform.position = position;
        yield return null;
    }
}


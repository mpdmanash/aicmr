﻿using UnityEngine;
using UnityEngine.VR.WSA.Input;
using HoloToolkit.Unity;

public class GazeGestureManager : MonoBehaviour
{
    public static GazeGestureManager Instance { get; private set; }
    // Represents the hologram that is currently being gazed at.
    public GameObject FocusedObject { get; private set; }
    public MeshClick mc;
    GestureRecognizer recognizer;
    Vector3 exportPosition;
    private bool exportFinished = false; // Reference sent to get event finished feedback
    private bool firstClick = true;

    // Use this for initialization
    void Start()
    {
        Instance = this;

        // Set up a GestureRecognizer to detect Select gestures.
        recognizer = new GestureRecognizer();
        recognizer.TappedEvent += (source, tapCount, ray) =>
        {
            mc = new MeshClick();
            mc.write_local = false;
            exportPosition = Camera.main.transform.position;
            mc.exportMesh(ref exportFinished, firstClick);
            firstClick = false;
        };
        recognizer.StartCapturingGestures();
    }

    private void AddSphereMarker()
    {
        GameObject new_sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Renderer rend = new_sphere.GetComponent<Renderer>();
        rend.material.shader = Shader.Find("Legacy Shaders/Diffuse");
        rend.material.color = Color.blue;
        new_sphere.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        new_sphere.transform.position = exportPosition;
    }

    // Update is called once per frame
    void Update()
    {
        // After data export is finished, visualize a sphere on the user's position during click
        if (exportFinished)
        {
            exportFinished = false;
            AddSphereMarker();
        }
    }
}
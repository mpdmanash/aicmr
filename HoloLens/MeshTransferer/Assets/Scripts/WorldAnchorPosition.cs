﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR.WSA.Persistence;
using UnityEngine.VR.WSA;

public class WorldAnchorPosition : MonoBehaviour {

    WorldAnchorStore mystore;

    // Called by GazeGestureManager when the user performs a Select gesture
    void Start()
    {
        Debug.Log("Start ===========");
        //moveObject();
        //WorldAnchorStore.GetAsync(AnchorStoreLoaded);
    }
    private void AnchorStoreLoaded(WorldAnchorStore store)
    {
        mystore = store;
        GameObject new_sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        new_sphere.GetComponent<Renderer>().material.color = Color.green;
        Renderer rend = new_sphere.GetComponent<Renderer>();
        rend.material.shader = Shader.Find("Legacy Shaders/Diffuse");
        rend.material.color = Color.green;

        GameObject bn = Instantiate(Resources.Load("CMUbanner")) as GameObject;

        //SaveAnchor();
        LoadAnchors(new_sphere, bn);
    }

    private void LoadAnchors(GameObject go, GameObject bn)
    {
        WorldAnchor anchor = mystore.Load("UniqueAnchorName", go);
        if (!anchor)
        {
            Debug.Log("could not load anchor");
        }
        else
        {
            Debug.Log("Anchor is loaded");
            var thisanchor = go.GetComponent<WorldAnchor>();
            Vector3 positionSphere = new Vector3(-5.004f, 1.3080f, -7.882679f);
            Vector3 vecOrientBanner = new Vector3(0.97264f, 0f, -0.6345f);
            Vector3 positionBanner = new Vector3(-0.1046f, 2.045f, -12.307740f);          


            if (thisanchor)
            {
                Debug.Log("Attempting to delete");                
                DestroyImmediate(thisanchor);                

                vecOrientBanner = go.transform.TransformDirection(vecOrientBanner);
                Quaternion rotationBanner = Quaternion.identity;
                rotationBanner.eulerAngles = new Vector3(0, Quaternion.FromToRotation(new Vector3(1.0f, 0.0f, 0.0f), vecOrientBanner).eulerAngles.y, 0);
                positionBanner = go.transform.TransformPoint(positionBanner);
                bn.transform.position = positionBanner;
                bn.transform.rotation = rotationBanner;

                Vector3 new_position = go.transform.TransformPoint(positionSphere);
                go.transform.position = new_position;

                Debug.Log("deleted anchor");
            }
            

            Vector3 rP = go.transform.position;
            Quaternion rO = go.transform.rotation;
            Debug.Log("Position: " + rP.x.ToString() + " " + rP.y.ToString() + " " + rP.z.ToString());
            Debug.Log("Orientation: " + rO.x.ToString() + " " + rO.y.ToString() + " " + rO.z.ToString() + " " + rO.w.ToString());
        }
    }

    private void SaveAnchor()
    {
        bool retTrue;
        var anchor = gameObject.AddComponent<WorldAnchor>();

        // Remove any previous worldanchor saved with the same name so we can save new one
        mystore.Delete("UniqueAnchorName");
        retTrue = mystore.Save("UniqueAnchorName", anchor);
        if (!retTrue)
        {
            Debug.Log("Anchor save failed.");
        }
        else
        {
            Debug.Log("Anchor is saved");
            Vector3 rP = anchor.transform.position; //.InverseTransformPoint(origin);
            Quaternion rO = anchor.transform.rotation;
            Debug.Log("Position: " + rP.x.ToString() + " " + rP.y.ToString() + " " + rP.z.ToString());
            Debug.Log("Orientation: " + rO.x.ToString() + " " + rO.y.ToString() + " " + rO.z.ToString() + " " + rO.w.ToString());
        }
    }

    private void ClearAnchor()
    {
        WorldAnchor anchor = gameObject.GetComponent<WorldAnchor>();
        if (anchor)
        {
            // remove any world anchor component from the game object so that it can be moved
            DestroyImmediate(anchor);
        }
    }

    private void moveObject()
    {
        Debug.Log("Moving object ===========");
        Vector3 position;
        position.y = 0.623512f;
        position.x = -1.325636f;
        position.z = 0.448886f;
        gameObject.transform.position = position;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
